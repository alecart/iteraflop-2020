<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <title>IteraFlop</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/album/">

    <!-- Bootstrap core CSS <link href="bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    -->
    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.4/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.4/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.4/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.4/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="album.css"  type="text/css" rel="stylesheet">
    <link href="bootstrap.min.css"  type="text/css" rel="stylesheet">
  </head>
  <body>
    <header><!--
  <div class="collapse bg-dark" id="navbarHeader">
    <div class="container">
      <div class="row">
        <div class="col-sm-8 col-md-7 py-4">
          <h4 class="text-white">About</h4>
          <p class="text-muted">Add some information about the album below, the author, or any other background context. Make it a few sentences long so folks can pick up some informative tidbits. Then, link them off to some social networking sites or contact information.</p>
        </div>
        <div class="col-sm-4 offset-md-1 py-4">
          <h4 class="text-white">Contact</h4>
          <ul class="list-unstyled">
            <li><a href="#" class="text-white">Follow on Twitter</a></li>
            <li><a href="#" class="text-white">Like on Facebook</a></li>
            <li><a href="#" class="text-white">Email me</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div> -->
  <div class="navbar navbar-light white shadow-sm">
    <div class="container d-flex justify-content-between">
      <a href="#" class="navbar-brand d-flex align-items-center black">
        <strong >ITERAFLOP</strong>
      </a><!--
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>-->
    </div>
  </div>
</header>

<main role="main">

  

  <div class="album py-5 bg-light">
    <div class="container">


            <section class="jumbotron text-center">
                    <div class="container">
                      <h1>A propos d'IteraFlop</h1>
                      <p class="lead text-muted">Studio conseil et développement de bug en mode pas trop Agile.</p>
                
                    </div>
                  </section>

      <div class="row">
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
                <img class="bd-placeholder-img card-img-top" width="100%" height="100%" src="lecart.jpg">
            <div class="card-body">
                <h2>Alan Lecart</h2>
              <p class="card-text">Co-gérant / CTO</p>
            </div>
          </div>
        </div>

        <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                <img class="bd-placeholder-img card-img-top" width="100%" height="100%" src="keklik.jpg">
                  <div class="card-body">
                          <h2>Mevlut Keklik</h2>
                          <p class="card-text">Co-gérant / CEO</p>
                         
                  </div>
                </div>
              </div>

        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
                <img class="bd-placeholder-img card-img-top" width="100%" height="100%" src="capart.jpg">
            <div class="card-body">
                    <h2>Antoni Capart</h2>
                    <p class="card-text">Lead Développeur</p>
                   
            </div>
          </div>
        </div>

        <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                        <img class="bd-placeholder-img card-img-top" width="100%" height="100%" src="pecro.jpg">
                  <div class="card-body">
                          <h2>Kevin Pécro</h2>
                          <p class="card-text">Développeur</p>
                         
                  </div>
                </div>
              </div>

        <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                    <img class="bd-placeholder-img card-img-top" width="100%" height="100%" src="lorne.jpg">
                 <div class="card-body">
                       <h2>Alexandre Lorne</h2>
                         <p class="card-text">Développeur</p>
                             
                 </div>
             </div>
        </div>

        <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                        <img class="bd-placeholder-img card-img-top" width="100%" height="100%" src="laurent.jpg">
                     <div class="card-body">
                           <h2>Albain Laurent</h2>
                             <p class="card-text">Comptable</p>
                                 
                     </div>
                 </div>
            </div>

        <div class="col-md-4">
               <div class="card mb-4 shadow-sm">
                    <img class="bd-placeholder-img card-img-top" width="100%" height="100%" src="collin.jpg">
                 <div class="card-body">
                          <h2>Victor Collin</h2>
                          <p class="card-text">Développeur</p>
                                     
                  </div>
              </div>
          </div>

        <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                <img class="bd-placeholder-img card-img-top" width="100%" height="100%" src="leleu.jpg">
                     <div class="card-body">
                           <h2>Kevin Leleu</h2>
                             <p class="card-text">Technicien de surface</p>
                                 
                     </div>
                 </div>
            </div>

        <div class="col-md-4">
                    <div class="card mb-4 shadow-sm">
                    <img class="bd-placeholder-img card-img-top" width="100%" height="100%" src="clerentin.PNG">
                         <div class="card-body">
                            <h2>Arnaud Clérentin</h2>
                            <p class="card-text">Consultant UNIX</p>
                                     
                    </div>
              </div>
         </div>

</main>

<footer class="text-muted">
  <div class="container">
    <p>Alan LECART &copy; Fait avec amour pour la Nightcode 2019</p>
    <p>Purement parodique et sans aucune méchancenté ❤️</p>
  </div>
</footer>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="/docs/4.4/dist/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script></body>
</html>
