<?php

require_once 'vendor/autoload.php';

Flight::route('/', function(){
    echo 'Nightcode';
});

Flight::route('GET /api/greetings', function(){
    
    if(Flight::request()->url == "/api/greetings")
    {
        Flight::json(array("message" => "hello, world"),200);

    }else if(Flight::request()->url == "/api/greetings?name="){

        Flight::json(array("message" => "hello, world"),400);
    }else{
        $id = Flight::request()->query["name"];
        Flight::json(array("message" => "hello, $id"));
    }
    
    
});

Flight::route('POST /api/greetings', function(){
    $name = Flight::request()->data->name;
    if($name == "")
    {
        Flight::json(array("message" => "hello, world"),400);
    }else{
        Flight::json(array("message" => "hello, $name"));
    }
});

Flight::route('GET /api/secret-base64', function(){
    Flight::json("", 405);
});

Flight::route('POST /api/secret-base64', function(){
    $brut = Flight::request()->getBody();
    $json = json_decode($brut);
    if($brut == "")
    {
        Flight::halt(400);
        echo "";
    }
    else
    {
        Flight::json(array("text" => base64_decode($json->{'to-translate'})));
    }
});

Flight::route('GET /api/secret-reduced', function(){
    Flight::json("", 405);
});

Flight::route('POST /api/secret-reduced', function(){
    $brut = Flight::request()->getBody();

    if(empty($brut))
    {
        Flight::halt(400);
        echo "";
    }
    else
    {
        $brut = json_decode($brut)->{"to-translate"};

        if (strlen($brut) > 2)
        {
            for ($i = 0; $i<strlen($brut) - 1; $i++)
            {
                if ($brut[$i] != $brut[$i+1] )
                    $array[$i]=$brut[$i];
                else
                    $i++;
            }

            if (substr($brut, -1) != substr($brut, -2, 1))
                $array[] = substr($brut, -1);

            if (!empty($array))
                $result = implode($array);
            else
                $result = "";
        }
        else
        {
            $result = $brut;
        }

        Flight::json(array("text" => str_replace("\"", "", $result)));
    }
});

Flight::route('POST /api/expense-report', function(){
    if(Flight::request()->url == "/api/expense-report") {
        $data = Flight::request()->getBody();

        $data = str_replace(" ", "", $data);
        $data = str_replace("\n", "", $data);
        $data .= "+";
        $length = strlen($data);


        try {
            if (preg_match("#^[\d\s+-]*$#", $data) === 1) {
                $i = 0;

                if ($data[$i] != "-") {
                    $total = $data[$i];
                } else {
                    $i += 1;
                    $total = ($data[$i] * -1);
                }

                while ($data[$i + 1] != "+" && $data[$i + 1] != "-") {
                    $i += 1;
                    $total .= $data[$i];
                }
                $i += 1;

                while ($i <= $length - 2) {

                    $signe = $data[$i];//on récupère le + / -
                    $i += 1;
                    $calcul = $data[$i];

                    while ($data[$i + 1] != "+" && $data[$i + 1] != "-") {
                        $i += 1;
                        $calcul .= $data[$i];
                    }

                    if ($signe == "+") {
                        $total += $calcul;
                    } else {
                        $total -= $calcul;
                    }
                    $i += 1;
                }
            }
            Flight::json(array("value" => $total));
        } catch (Exception $e) {
            Flight::json("", 400);
        }
    }
    else
    {
        $data = Flight::request()->getBody();

        $array_data = preg_split('/ /', $data, -1, PREG_SPLIT_NO_EMPTY);

        try {
            // array_push et array_pop
            $nombre = array();
            for ($i = 0; $i < count($array_data); $i++)
            {
                if(preg_match("/[0-9]+/", $array_data[$i]) === 1) {
                   array_push($nombre, intval($array_data[$i]));
                } else {
                    $b = array_pop($nombre);
                    $a = array_pop($nombre);
                    $operator = $array_data[$i];
                    if ($operator == "-") {
                        $calc = $a - $b;
                    } elseif ($operator == "+") {
                        $calc = $a + $b;
                    }
                    array_push($nombre, $calc);
                }
            }

            Flight::json(array("value" => array_pop($nombre)));
        } catch (Exception $e) {
            Flight::json("", 400);
        }

    }
});

Flight::route('POST /api/drawing', function(){
    $matrice = Flight::request()->data->payload;
    print_r($matrice);
    if($matrice != null){
        for($i=0;$i<count($matrice);$i++){
            for($j=0;$j<count($matrice);$j++){
                if($matrice[$i][$j]!=0 && $matrice[$i][$j]!=1){
                    $flag = true; 
                }
                else{
                    $flag = false; 
                }
            }  
        }
        if(!$flag){
            $image = imagecreate(count($matrice),count($matrice));
            for($i=0;$i<count($matrice);$i++){ //Creation de l'image
                for($j=0;$j<count($matrice);$j++){
                    if($matrice[$j][$i] == 0){
                        $color = imagecolorallocate($image,0,0,0);
                    }
                    else{
                        $color = imagecolorallocate($image,255,255,255);
                    }
                    imagesetpixel($image,$i,$j,$color);
                }  
            }
            var_dump(get_resource_type($image));
            ob_start();
            imagepng($image);
            $resultat = ob_get_contents();
            ob_end_clean();
            $sortieb64 = base64_encode($resultat);
            echo $sortieb64;
        }
        else{
            echo "erreur dans le format de la matrice ou de sa valeur";
        }
    }
    else{
        echo "erreur";
    }
    

    // "lire" le body
    // Transformer la matrice en image
    // Transformer l'image en base64

   // $bs64img =  base64_encode($img); //transfo img en b64
    //echo $bs64img;
    Flight::json(array('image' => $sortieb64));
});


Flight::start();